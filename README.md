<h1> Requirements </h1>

- Ubuntu 20.04
- Node 10.19.0
- Npm 6.14.4
- NuxtJS 2.15.3
- Next-UI 0.9.0

<h1>Front-end Application Installation </h1>

In a new console we run the following to create a directory for the front-end application

```bash
mkdir front-end
cd front-end
```

Now we install the NodeJs server to run the front-end application written in Javascript. Note: Check that the versions match the requirements.

```bash
sudo apt-get install nodejs npm -y
```

We clone repository from the GitLab manager.

```bash
git clone https://gitlab.com/sdn-group/nuxt-front.git
```

Enter the front-end application.

```bash
cd nuxt-front
```

Install the necessary packages with the following command.

```bash
# install dependencies
npm install
```

We run the nodejs server of the front-end application.

```bash
# serve with hot reload at localhost:3000
npm run dev

# build for production and launch server
npm run build
npm run start

# generate static project
npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
