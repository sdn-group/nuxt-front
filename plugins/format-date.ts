import Vue from "vue";

Vue.filter("formatDate", (dateStr: string) =>
  Intl.DateTimeFormat("us-EN", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    hour12: false,
  }).format(new Date(dateStr))
);